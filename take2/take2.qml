import QtQuick 1.1

Rectangle {
    width: 360
    height: 360

    ListModel {
        id: myModel
        ListElement { direction: 1; message: "Hey!"; failedToSend: false; sender: "Martin Klapetek"}
        ListElement { direction: 1; message: "Check out <a href=\"http://foo.co.uk\">http://foo.co.uk/myImage</a>" ; failedToSend: false ; sender: "Martin Klapetek"; image:"http://b.vimeocdn.com/ts/201/743/201743480_640.jpg"}
        ListElement { direction: 0; message: "Dude, that's <b>awesome!</b>"; failedToSend:true ; sender: "David Edmundson"}
        ListElement { messageType: 1; message: "Dan Vratil has joined the room";}
        ListElement { direction: 1; message: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec accumsan sagittis dictum. In mauris massa, fringilla quis diam consequat, tincidunt consectetur tortor. Mauris lobortis scelerisque ante, a imperdiet leo placerat vitae. Donec mattis nunc quam, auctor pulvinar urna hendrerit sed. In malesuada porttitor arcu, ut venenatis dui commodo in. Nulla sed dolor eleifend, cursus nulla sed, tristique dolor. Vivamus non condimentum orci. Quisque nibh dui, adipiscing in porta at, molestie ut libero. Ut at ultrices libero. Phasellus quis rhoncus nibh. Donec pulvinar urna ut felis gravida, sed porta libero volutpat. Nullam placerat imperdiet nunc, sed consequat mi sodales non."; failedToSend:true ; sender: "Martin Klapetek"}
        ListElement { direction: 0; message: "ASDFSAF!";  failedToSend: false ; sender: "David Edmundson"}

    }

    Header {
        id:header
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        z: 1000
    }

    ListView { //or column + repeater?
        id:listView
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        model: myModel

        delegate: Loader {
            Component.onCompleted: {
                switch(model.messageType) {
                case 1:
                    source = "actionDelegate.qml";
                    break
                default:
                    source = "messageDelegate.qml";
                    break;
                }
            }
        }
        boundsBehavior: Flickable.StopAtBounds
    }

}
