// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    height: 80

    Image {
        anchors.fill: parent
        fillMode: Image.TileHorizontally
        source: "Resources/images/steelHeading.jpg"
    }

    Image {
        id: avatarImage

        fillMode: Image.PreserveAspectFit

        source: "/home/david/Pictures/webcam/picture_1.png"

        anchors {
            top: parent.top
            left: parent.left
            bottom:parent.bottom

            margins: 5
        }
    }

    Rectangle {
        color: "#d5d5d5"
        height:2
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }

//    color: "red"
}
