// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

MessageOut {
    color: model.direction === 0 ? "red" : "blue"
    direction: model.direction
    width: parent.width
    content: TextEdit { //use text edit so we can copy paste..
            id: _text
            text: model.message
            readOnly: true
            selectByMouse: true
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            anchors.margins: 13

            height: paintedHeight

            wrapMode: Text.WordWrap
        }

}
